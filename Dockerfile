FROM        mlhamel/mlhamel.base

WORKDIR     /home
RUN         mkdir /home/arrak.org
COPY        . /home/arrak.org/
RUN         cd /home/arrak.org && python3 setup.py develop

EXPOSE      8000

WORKDIR     /home/arrak.org
CMD         DJANGO_SETTINGS_MODULE=arrak.org.settings.development django-admin runserver 0.0.0.0:8000