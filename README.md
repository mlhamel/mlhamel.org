# README #

arrak.org is the old name my main website. Now you can reach it as mlhamel.org.
It contain a blog, powered by Pelican, a static website generator and just a quick
presentation of myself.

### How do I get set up? ###

Make sure you have on your machine the following things:

* python3
* docker

It is pretty easy to setup, just follow those steps:

Clone the source:

```
$ git clone git@bitbucket.org:mlhamel/arrak.org.git
```

Create the virtualenv:

```
$ virtualenv ~/venvs/arrak.org && source ~/venvs/arrak.org/bin/activate
```

Install the python dependencies

```
$ python setup.py develop
```

Build the docker image:

```
$ make
...
Removing intermediate container 1f28b7d3a324
Step 7 : CMD DJANGO_SETTINGS_MODULE=arrak.org.settings.development django-admin runserver 0.0.0.0:8000
---> Running in 0c2a016d5195
---> 4ba62691fb74
Removing intermediate container 0c2a016d5195
Successfully built 4ba62691fb74
```

Tag the image as latest:

```
$ make tag revision=4ba62691fb74
```

Push the image to your docker hub account

```
$ make push
```
