try:
    from setuptools import setup, find_packages
except ImportError:
    from ez_setup import use_setuptools
    use_setuptools()
    from setuptools import setup, find_packages

version = "1.5.3"

setup(name="arrak.org",
      version=version,
      description="",
      long_description="",
      classifiers=[],
      keywords="",
      author="Mathieu Leduc-Hamel",
      author_email="marrakis@gmail.com",
      license="",
      packages=find_packages(exclude=["ez_setup", "examples", "tests"]),
      namespace_packages=["arrak", "arrak.org"],
      include_package_data=True,
      zip_safe=False,
      install_requires=[
          "bumpversion",
          "django",
          "Markdown",
          "pelican",
          "werkzeug",
      ],
      entry_points={
          "django.apps": [
              "arrak.org = arrak.org"
          ]})
