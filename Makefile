ifndef message
	message = "Updating web app"
endif

ifndef repos
	repos = mlhamel/mlhamel.org
endif

.SHELLFLAGS = -e
.PHONY: docker-build
.NOTPARALLEL:

default: build
build: docker-build
commit: docker-commit
push: docker-push
tag: docker-tag
docker-build: do-docker-build
docker-commit: do-docker-commit
docker-tag: do-docker-tag
docker-push: do-docker-push

do-docker-build:
	docker build -t mlhamel.org --no-cache --rm . | tee build.log || exit 1

do-docker-commit:
	docker commit -m $(message) $(revision) $(repos)

do-docker-tag:
	docker tag -f $(revision) $(repos)

do-docker-push:
	docker push $(repos)

# Version Bump using bumpversion
patch:
	bumpversion patch
major:
	bumpversion major
minor:
	bumpversion minor

# Compilation of the blog posts
compile:
	pelican

# run de development environment
run:
	DJANGO_SETTINGS_MODULE=arrak.org.settings.development django-admin runserver
