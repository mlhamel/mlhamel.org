# Django settings for agenda project.
import arrak.org
import os

DEBUG = True
PROJECT_ROOT = os.path.dirname(arrak.org.__file__)

ADMINS = (
    ('Mathieu Leduc-Hamel', 'marrakis@gmail.com'),
)

MANAGERS = ADMINS

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(PROJECT_ROOT, 'dev.db')
    }
}

TIME_ZONE = 'America/Montreal'

LANGUAGE_CODE = 'fr-ca'

SITE_ID = 1

USE_I18N = True

MEDIA_ROOT = os.path.join(PROJECT_ROOT, 'media')

MEDIA_URL = ''

ADMIN_MEDIA_PREFIX = '/media/admin/'

SECRET_KEY = '2oaw+105(kzy2ybr58@%1u&w_!5r6)ykg6wk9+=3+m#j*@7!n_'

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
)

ROOT_URLCONF = 'arrak.org.urls'

STATIC_URL = '/media/'
STATICFILES_DIRS = [
    os.path.join(PROJECT_ROOT, 'media'),
]

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            os.path.join(PROJECT_ROOT, 'templates'),
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.messages',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.admin',
    'django.contrib.humanize',
    'django.contrib.sitemaps',
    'django.contrib.staticfiles',
)
