from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings

from arrak.org import views

import os

admin.autodiscover()

urlpatterns = [
    url(r'^$', views.index),
    url(r'^en/', views.index),

    url(r'^blog/$', views.blog_index),
    url(r'^blog/(.*)/', views.blog_post),
    url(r'^admin/', include(admin.site.urls)),
]
