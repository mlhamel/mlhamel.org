Title: Joining Shopify
Date: 2015-07-10 18:00
Modified: 2015-07-10 18:00
Category: job
Tags: shopify, ajah
Slug: joining-shopify
Authors: Mathieu Leduc-Hamel
Summary: Announcement, Joining Shopify

Whoot whoot ! Today, i've just completed my second week as a software developer at Shopify.

After three amazing years working with the great team at Ajah, I decided it was time for me to experience new challenges, to discover a new kind of environment, and joining a bigger team. I'm super excited by the journey ahead of me, with my new colleagues from Montreal, Ottawa and Toronto.

I'm already learning a lot: I'm going to discover new languages, new tools like Docker, and new platform like Ruby on Rails (yes, yes!). I'm just starting to understand the complexity and the beauty of the Shopify platform.

What does it change for Montreal-Python ? Nothing. I still love Python, the language and the community are still going to be part of me and my daily life. Yes ! I'm still planning to organize monthy event. Yes ! i'm still going to be part of the PyCon organizers.

For now I can't give any details about the projects I'm going to work on, but my plan is to try to talk about it and to share what I'm going to learn with you!

I'm slowly discovering what the web and the retail industry is becoming and will become in the next few years... and it is exciting :)

à bientot !

[![shopify](/media/img/shopify-logo.png)](https://shopify.com/)
