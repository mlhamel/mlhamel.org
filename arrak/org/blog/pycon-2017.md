Title: PyCon 2017: Not this time... :(
Date: 2017-01-24 20:30:00
Category: conference
Tags: pycon, baby
Slug: pycon-2017-not-this-time
Summary: I'm not gonna make it for PyCon 2017

I'm attending PyCon since Atlanta 2012. I'm a regular of this amazing largest gathering of Python Programmers in the world since then. But, sadly, i'm not gonna make this year. But I promise, I've got a good reason: if things goes the way it's supposed to go; my **girl friend Claudia and me are gonna become new parents by the end of March**!!!!

I don't want to miss those first few weeks of life of this new human being: it's simply too important :) I'm gonna miss you all, my friends the Pythonistas: my friends from all around the World.


But no worries, I'm still gonna watch the talks on [pyvideo.org](http://pyvideo.org) while feeding him :)


See you at [PyCon Canada](https://2017.pycon.ca/) in November, in Montreal!

[<img src="https://us.pycon.org/2017/site_media/static/img/pycon-cityscape.png" width="650px" />](https://us.pycon.org/2017/)

See you next at PyCon 2018, hopefully there will be daycare service :)
