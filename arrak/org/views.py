from django.shortcuts import render_to_response


def index(request):
    return render_to_response("index.html")


def blog_index(request):
    return render_to_response("blog/index.html")


def blog_post(request, slug):
    return render_to_response("blog/{0}".format(slug))
