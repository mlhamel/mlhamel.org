#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = u'Mathieu Leduc-Hamel'
SITENAME = u'mlhamel.org'
SITEURL = ''

PATH = 'arrak/org/blog'
OUTPUT_PATH = 'arrak/org/templates/blog'

TIMEZONE = 'America/Montreal'

DEFAULT_LANG = u'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

LINKS = ()
SOCIAL = ()
DEFAULT_PAGINATION = 10
RELATIVE_URLS = True

THEME='themes/mlhamel'

DEFAULT_DATE_FORMAT = '%Y/%m/%d'
